# frozen_string_literal: true

require_relative 'main'

album = Album.new('album')
group = Group.new('group')

puts album.inspect
puts group.inspect

puts '=' * 20

puts album.title
puts group.title
puts group.leader

# album_data = File.read('./data/album.json')
# group_data = File.read('./data/group.json')

# puts Oj.load(album_data)
# puts Oj.load(group_data)

# album = Album.new

# Oj.load(album_data).each do |method, value|
#   # album.send "#{method}=", value # первый вариант
#   album.instance_variable_set :"@#{method}", value
# end

# puts album.inspect
